const s ='Hello world';

//this is not a method
console.log(s.length);

console.log(s.toUpperCase());

console.log(s.toLowerCase());

//return 1st-6th letter
console.log(s.substring(0,5));

// split by letter and return an array ["H", "e", "l", "l", "o", " ", "w", "o", "r", "l", "d"] include space
console.log(s.split('')); 

// console.log('my name is '+ name+'and I am ' +age);
// const hello = `my name is $(name) and I am $(age)`;

//split by ,+space
const str = 'hello, my, name, is';
console.log(str.split(', '));

//difine an array, different types of values an in one array
const fruit = ['apple','oranges', 'pears', 10, true]
//add elements at the end
fruit.push('mangos');
//add elements at the begin
fruit.unshift('strawberry');
//take the last element off
fruit.pop();

//check whether is an array
console.log(Array.isArray(fruit));

//find the index of an element
console.log()

//const array is able to add/ change elements in it, but can not re-define the whole array
fruit[2]='aaa';
console.log(fruit[2]); // aaa will return  

// const name = 'chloe';
// console.log(typeof name);

const num = 3.141748297419;
console.log(parseFloat(num));

console.log(Math.PI);
console.log(Math.round(3.4));
console.log(Math.ceil(3.4));
console.log(Math.floor(3.8));
console.log(Math.sqrt(64));
console.log(Math.abs(-5));
console.log(Math.pow(3,3));
console.log(Math.min(3,5,6,44,0,77,88));
console.log(Math.floor(Math.random()*20+1));

var a =  'Lei';
a += 'Tian';
console.log(a);

console.log(a.indexOf('i'));
console.log(a.charAt(a.length-1));

var name = 'Chloe';
var age = 24;
var job = 'web developer';
html = `
    <ul>
        <li>Name: ${name}</li>
        <li>Age: ${age}</li>
        <li>Job: ${job}</li>
    </ul>`
;
document.body.innerHTML = html;

const numbers = [1,2,3,4,5,6,7];
const numbers2 = [8,9,10,11,12,13];
console.log(numbers.length);
console.log(Array.isArray(numbers));
console.log(numbers[3]);

numbers[2] = 3.1
console.log(numbers);

console.log(numbers.indexOf(4));
numbers.push(8)
console.log(numbers);

numbers.unshift(0)
console.log(numbers);

numbers.pop();
console.log(numbers);

numbers.shift();
console.log(numbers);

numbers.splice(1,1);
console.log(numbers);

numbers.reverse();
console.log(numbers);

var arr = numbers.concat(numbers2);
console.log(arr);

arr = arr.sort(function(x,y){
   return x-y;
})
console.log(arr);

arr = arr.sort(function(x,y){
    return y-x;
})
console.log(arr);