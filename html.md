# form/get/post
1. form
```
<form action = www.google.com method = "get">
    <p> 
        <input type = "text" name = "login">
    </p>
    <p>
        <input type = "password" name = "pass">
    </p>
    <p>
        <input type = "submit">
    </p>
</form>
```
2. 
- get- get data from server // 可以在地址栏看到
- post- send data to server // 不可在地址栏看到
- get 不能发送大量数据
- post 可发送大数据

3. 
- css cascading style sheet 层叠样式表
4. 容器
- \<spam\>
容器标签，包裹文本，用于给文本增加样式
因为 \<p\> 标签包裹文本，独占一行
- \<div\> 容器标签，包裹任何内容，甚至包裹本身
默认div 宽度100% 高度0
- \<div style = "line-height: 80px; text-align: center;" \>
容器内部字体居中，在div标签设置line-height属性为div高度即可
5. body
- \<body style= "margin: 0;"\> body默认空隙消除
6. 内部样式
